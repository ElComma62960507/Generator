class Generator {
    generate(params = {}) {
        throw 'Abstract';
    }

    validate(value) {
        throw 'Abstract';
    }

    params() {
        return {};
    }

    toJSON() {
        return this.params();
    }
}

module.exports = Generator;
