const _each = (obj, callable) => {
    const ret = [];

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            ret.push(callable(obj[key], key));
        }
    }

    return ret;
};  // TODO use vanillin


module.exports = class Ean13Barcode {
    static get startMarker() {
        return [
            [1, true],
            [0, true],
            [1, true],
        ];
    }

    static get centerMarker() {
        return [
            [0, true],
            [1, true],
            [0, true],
            [1, true],
            [0, true],
        ];
    }

    static get endMarker() {
        return [
            [1, true],
            [0, true],
            [1, true],
        ];
    }

    static get firstGroupStructure() {
        return {
            0: 'LLLLLL',
            1: 'LLGLGG',
            2: 'LLGGLG',
            3: 'LLGGGL',
            4: 'LGLLGG',
            5: 'LGGLLG',
            6: 'LGGGLL',
            7: 'LGLGLG',
            8: 'LGLGGL',
            9: 'LGGLGL',
        };
    }

    static get encoding() {
        return {
            0: {'L': '0001101', 'G': '0100111', 'R': '1110010'},
            1: {'L': '0011001', 'G': '0110011', 'R': '1100110'},
            2: {'L': '0010011', 'G': '0011011', 'R': '1101100'},
            3: {'L': '0111101', 'G': '0100001', 'R': '1000010'},
            4: {'L': '0100011', 'G': '0011101', 'R': '1011100'},
            5: {'L': '0110001', 'G': '0111001', 'R': '1001110'},
            6: {'L': '0101111', 'G': '0000101', 'R': '1010000'},
            7: {'L': '0111011', 'G': '0010001', 'R': '1000100'},
            8: {'L': '0110111', 'G': '0001001', 'R': '1001000'},
            9: {'L': '0001011', 'G': '0010111', 'R': '1110100'},
        };
    };

    constructor(code) {
        this.code = code;
        this.firstDigit = code[0];
        this.group1 = code.slice(1, 7);
        this.group2 = code.slice(7, 13);

        this.marks = [
            ...Ean13Barcode.startMarker,
            ...this.encodeGroup(this.group1, Ean13Barcode.firstGroupStructure[this.firstDigit]),
            ...Ean13Barcode.centerMarker,
            ...this.encodeGroup(this.group2, 'RRRRRR'),
            ...Ean13Barcode.endMarker,
        ];
    }

    encodeGroup(group, structure) {
        const output = [];

        _each(group, (char, i) => {
            _each(Ean13Barcode.encoding[parseInt(char)][structure[i]], value => {
                output.push([parseInt(value), false]);
            });
        });

        return output;
    }

    toSvg() {
        //  width="115" height="70"
        let svg = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 115 70">';

        _each(this.marks, ([mark, tall], i) => {
            if (mark) {
                svg += `<rect x="${10 + parseInt(i)}" y="10" width="1" height="${tall ? 50 : 40}" style="fill:black;stroke: none;stroke-width:0" />`;
            }
        });

        svg += `<text x="5" y="57" dominant-baseline="middle" text-anchor="middle" style="font-family: Arial sans-serif; font-size: 10px;">${this.firstDigit}</text>`;
        svg += `<text x="35" y="57" dominant-baseline="middle" text-anchor="middle" style="font-family: Arial sans-serif; font-size: 10px;">${this.group1}</text>`;
        svg += `<text x="82" y="57" dominant-baseline="middle" text-anchor="middle" style="font-family: Arial sans-serif; font-size: 10px;">${this.group2}</text>`;

        svg +='</svg>';

        return svg;
    }
};
